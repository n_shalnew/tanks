import java.util.ArrayList;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2i;

public class Base extends PhysicalObject {

    Base(int x, int y, int width, int height) {
        super(x, y, width, height);
        hp = 1;
    }
    Random random = new Random();

    @Override
    void update(ArrayList<PhysicalObject> allObjects) {
        if (hp <= 0) {
            isRemove = true;
            System.exit(1);
        }

    }

    @Override
    void draw() {
        glColor3f(random.nextInt(2), random.nextInt(2), random.nextInt(2));
        glBegin(GL_POLYGON);
        glVertex2i(x - wigth / 2, y - height / 2);
        glVertex2i(x - wigth / 2, y + height / 2);
        glVertex2i(x + wigth / 2, y + height / 2);
        glVertex2i(x + wigth / 2, y - height / 2);
        glEnd();
    }
}
