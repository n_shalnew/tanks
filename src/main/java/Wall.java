import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2i;

public class Wall extends PhysicalObject {

    Wall(int x, int y, int width, int height) {
        super(x, y, width, height);
        hp = 2;
    }


    @Override
    void update(ArrayList<PhysicalObject> allObjects) {
        if (hp <= 0) {
            isRemove = true;
        }
    }

    @Override
    void draw() {
        if (hp > 0) {
            glColor3f(1f, 0f, 0.1f);
            glBegin(GL_POLYGON);
            glVertex2i(x - wigth / 2, y - height / 2);
            glVertex2i(x - wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y - height / 2);
            glEnd();
            glColor3f(0f, 0f, 0.1f);
            glBegin(GL_LINE_LOOP);
            glVertex2i(x - wigth / 2, y - height / 2);
            glVertex2i(x - wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y - height / 2);
            glEnd();
            if (hp <= 1) {
                glColor3f(0, 0, 0);
                glBegin(GL_LINES);
                glVertex2i(x - wigth / 2, y - height / 4);
                glVertex2i(x + wigth / 2, y - height / 4);
                glVertex2i(x - wigth / 2, y + height / 4);
                glVertex2i(x + wigth / 2, y + height / 4);

                glEnd();
            }
        }
    }


}
