import java.util.ArrayList;
import java.util.Arrays;

public class World {

    static int cellSize = 40;// степень двойки умноженная на 10
    static int bulletSize = 10;


    static int map[][] = new int[][]{
            {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
            {9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 9},//тут угол
            {9, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 9},
            {9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9},
            {9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 8, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9},
            {9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 9},//тут угол
            {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},

    };

    static void generateMap(ArrayList<PhysicalObject> allObjects) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == 1) {//стена
                    Wall wall = new Wall((j + 1) * World.cellSize, (i + 1) * World.cellSize, World.cellSize, World.cellSize);
                    allObjects.add(wall);
                }
                if (map[i][j] == 8) {//база
                    generateBase((j + 1) * World.cellSize, (i + 1) * World.cellSize, allObjects);
                }
                if (map[i][j] == 2) {//центр руин
                    ruines1((j + 1) * World.cellSize, (i + 1) * World.cellSize, allObjects);
                }
                if (map[i][j] == 9) {//неразрушаемые стены
                    RootWall rootWall = new RootWall((j + 1) * World.cellSize, (i + 1) * World.cellSize, World.cellSize, World.cellSize);
                    allObjects.add(rootWall);
                }
            }
        }
    }

    static void ruines1(int x, int y, ArrayList<PhysicalObject> allObjects) {
        Wall wall1 = new Wall(x - World.cellSize * 2, y - World.cellSize * 2, World.cellSize, World.cellSize);
        Wall wall2 = new Wall(x - World.cellSize, y - World.cellSize * 2, World.cellSize, World.cellSize);
        Wall wall3 = new Wall(x + World.cellSize, y - World.cellSize * 2, World.cellSize, World.cellSize);
        Wall wall4 = new Wall(x + World.cellSize * 2, y - World.cellSize * 2, World.cellSize, World.cellSize);
        Wall wall5 = new Wall(x - World.cellSize * 2, y - World.cellSize, World.cellSize, World.cellSize);
        Wall wall6 = new Wall(x + World.cellSize * 2, y - World.cellSize, World.cellSize, World.cellSize);
        Wall wall7 = new Wall(x - World.cellSize * 2, y + World.cellSize, World.cellSize, World.cellSize);
        Wall wall8 = new Wall(x + World.cellSize * 2, y + World.cellSize, World.cellSize, World.cellSize);
        Wall wall9 = new Wall(x - World.cellSize * 2, y + World.cellSize * 2, World.cellSize, World.cellSize);
        Wall wall10 = new Wall(x - World.cellSize, y + World.cellSize * 2, World.cellSize, World.cellSize);
        Wall wall11 = new Wall(x + World.cellSize, y + World.cellSize * 2, World.cellSize, World.cellSize);
        Wall wall12 = new Wall(x + World.cellSize * 2, y + World.cellSize * 2, World.cellSize, World.cellSize);
        allObjects.add(wall1);
        allObjects.add(wall2);
        allObjects.add(wall3);
        allObjects.add(wall4);
        allObjects.add(wall5);
        allObjects.add(wall6);
        allObjects.add(wall7);
        allObjects.add(wall8);
        allObjects.add(wall9);
        allObjects.add(wall10);
        allObjects.add(wall11);
        allObjects.add(wall12);
    }

    static void ruines2(int x, int y, ArrayList<PhysicalObject> allObjects) {
        Wall[] walls = new Wall[10];
        walls[0] = new Wall(x - World.cellSize, y - World.cellSize * 2, World.cellSize, World.cellSize);
        walls[1] = new Wall(x - World.cellSize, y - World.cellSize, World.cellSize, World.cellSize);
        walls[2] = new Wall(x - World.cellSize, y, World.cellSize, World.cellSize);
        walls[3] = new Wall(x - World.cellSize, y + World.cellSize, World.cellSize, World.cellSize);
        walls[4] = new Wall(x, y + World.cellSize, World.cellSize, World.cellSize);
        walls[5] = new Wall(x + World.cellSize, y + World.cellSize, World.cellSize, World.cellSize);
        walls[6] = new Wall(x + World.cellSize * 2, y + World.cellSize, World.cellSize, World.cellSize);
        walls[7] = new Wall(x + World.cellSize, y - World.cellSize * 2, World.cellSize, World.cellSize);
        walls[8] = new Wall(x + World.cellSize * 2, y - 80, World.cellSize, World.cellSize);
        walls[9] = new Wall(x + World.cellSize * 2, y - World.cellSize, World.cellSize, World.cellSize);
        allObjects.addAll(Arrays.asList(walls));
    }
/*не готовы*/
    static void ruines3(int x, int y, ArrayList<PhysicalObject> allObjects) {
        Wall[] walls = new Wall[7];
        walls[0] = new Wall(x - 120, y, 40, 40);
        walls[1] = new Wall(x - 80, y, 40, 40);
        walls[2] = new Wall(x - 40, y, 40, 40);
        walls[3] = new Wall(x, y, 40, 40);
        walls[4] = new Wall(x + 40, y, 40, 40);
        walls[5] = new Wall(x + 80, y, 40, 40);
        walls[6] = new Wall(x + 120, y, 40, 40);
        allObjects.addAll(Arrays.asList(walls));
    }
/*не готовы*/
    static void ruines5(int x, int y, ArrayList<PhysicalObject> allObjects) {
        Wall[] walls = new Wall[7];
        walls[0] = new Wall(x, y - 120, 40, 40);
        walls[1] = new Wall(x, y - 80, 40, 40);
        walls[2] = new Wall(x, y - 40, 40, 40);
        walls[3] = new Wall(x, y, 40, 40);
        walls[4] = new Wall(x, y + 40, 40, 40);
        walls[5] = new Wall(x, y + 80, 40, 40);
        walls[6] = new Wall(x, y + 120, 40, 40);
        allObjects.addAll(Arrays.asList(walls));
    }

    static void generateBase(int x, int y, ArrayList<PhysicalObject> allObjects) {
        Wall[] walls = new Wall[8];
        Base base = new Base(x, y, World.cellSize, World.cellSize);
        walls[0] = new Wall(x - World.cellSize, y - World.cellSize, World.cellSize, World.cellSize);
        walls[1] = new Wall(x, y - World.cellSize, World.cellSize, World.cellSize);
        walls[2] = new Wall(x + World.cellSize, y - World.cellSize, World.cellSize, World.cellSize);
        walls[3] = new Wall(x - World.cellSize, y, World.cellSize, World.cellSize);
        walls[4] = new Wall(x + World.cellSize, y, World.cellSize, World.cellSize);
        walls[5] = new Wall(x - World.cellSize, y + World.cellSize, World.cellSize, World.cellSize);
        walls[6] = new Wall(x, y + World.cellSize, World.cellSize, World.cellSize);
        walls[7] = new Wall(x + World.cellSize, y + World.cellSize, World.cellSize, World.cellSize);
        allObjects.addAll(Arrays.asList(walls));//добавляет все объект массива
        allObjects.add(base);
    }

    static void generateExtremeWalls(ArrayList<PhysicalObject> allObjects) {
        RootWall[] walls = new RootWall[102];
        for (int i = 1; i < 103; i++) {
            if (i < 32) {//32
                walls[i] = new RootWall(i * 40, 40, 40, 40);
                allObjects.add(walls[i]);
            }
            if (i > 32 && i < 51) {//19
                walls[i] = new RootWall(40, (i - 32) * 40, 40, 40);
                allObjects.add(walls[i]);
            }
            if (i > 52 && i < 83) {//31
                walls[i] = new RootWall((i - 51) * 40, 720, 40, 40);
                allObjects.add(walls[i]);
            }
            if (i > 82 && i < 100) {//19
                walls[i] = new RootWall(1240, (i - 81) * 40, 40, 40);
                allObjects.add(walls[i]);
            }
        }
    }
}
