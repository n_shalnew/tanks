import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.*;
import org.lwjgl.opengl.DisplayMode;

import java.util.ArrayList;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;

public class MainClass {
    private static int WIDTH = 1280;
    private static int HEIGHT = 768;
    private static String CAPTION = "Wooooow! Impressive";
    private static ArrayList<PhysicalObject> allObjects = new ArrayList<PhysicalObject>();

    public static void main(String[] args) {
        openWindow();

    }

    private static void openWindow() {
        Display.setTitle(CAPTION);
        try {
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        initializeOpenGl();

        Random random = new Random();

        /*тут конструкции*/
        World.generateMap(allObjects);

       // Tank tank1 = new Tank(40, 60, World.cellSize, World.cellSize);
        Tank tank1 = new Tank(80, 120, World.cellSize, World.cellSize);
        tank1.isPlayer = true;
        tank1.num = 1;
        tank1.hp = 500;
        tank1.setColor(0.635f, 0.250f, 0.866f);
        allObjects.add(tank1);

        //Tank tank2 = new Tank(200, 180, World.cellSize, World.cellSize);
        Tank tank2 = new Tank(200, 360, World.cellSize, World.cellSize);
        tank2.num = 2;

        tank2.setColor(0.854f, 0.780f, 0.043f);
        allObjects.add(tank2);

        //Tank tank3 = new Tank(440, 160, World.cellSize, World.cellSize);
        Tank tank3 = new Tank(440, 160, World.cellSize, World.cellSize);
        tank3.num = 3;
        tank3.setColor(0.796f, 0.784f, 0.764f);
        allObjects.add(tank3);

       // Tank tank4 = new Tank(540, 280, World.cellSize, World.cellSize);
        Tank tank4 = new Tank(1080, 560, World.cellSize, World.cellSize);
        tank4.num = 4;
        tank4.setColor(0.792f, 0.356f, 0.109f);
        allObjects.add(tank4);

        //Tank tank5 = new Tank(400, 60, World.cellSize, World.cellSize);
        Tank tank5 = new Tank(400, 120, World.cellSize, World.cellSize);
        tank5.num = 5;
        tank5.setColor(0.082f, 0.896f, 0.809f);
        allObjects.add(tank5);


        tank2.hp = 50;
        tank3.hp = 50;
        tank4.hp = 50;
        tank5.hp = 50;

        //World.generateExtremeWalls(allObjects);

        while (!Display.isCloseRequested()) {
            glClear(GL_COLOR_BUFFER_BIT);

            /*Тут отрисовка*/

            for (int i = 0; i < allObjects.size(); i++) {
                allObjects.get(i).draw();
            }
            //System.out.println("Now in scene " + allObjects.size());

            Display.update();
            /*тут движение*/
            for (int i = 0; i < allObjects.size(); i++) {
                allObjects.get(i).update(allObjects);
            }

            tank2.skyNet(allObjects);
            tank3.skyNet(allObjects);
            tank4.skyNet(allObjects);
            tank5.skyNet(allObjects);

            for (int i = 0; i < allObjects.size(); i++) {
                if (allObjects.get(i).isRemove)
                    allObjects.remove(i);
            }

            Display.sync(60);
        }
        Display.destroy();
        System.exit(0);
    }

    private static void initializeOpenGl() {
        /*Инициализация OpenGl*/
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, WIDTH, HEIGHT, 0, 1, -1);
        glMatrixMode(GL_MODELVIEW);
        /*------*/
    }


}
