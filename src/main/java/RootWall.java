import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2i;

public class RootWall extends Wall {

    RootWall(int x, int y, int width, int height) {
        super(x, y, width, height);
        hp = 400;
    }

    @Override
    void draw() {
        if (hp > 0) {
            glColor3f(.5f, 0.7f, 0.3f);
            glBegin(GL_POLYGON);
            glVertex2i(x - wigth / 2, y - height / 2);
            glVertex2i(x - wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y - height / 2);
            glEnd();
            glColor3f(0f, 0f, 0.1f);
            glBegin(GL_LINE_LOOP);
            glVertex2i(x - wigth / 2, y - height / 2);
            glVertex2i(x - wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y - height / 2);
            glEnd();

        }
    }
}
