import java.util.ArrayList;

public abstract class GameObject {

    abstract void update(ArrayList<PhysicalObject> allObjects);
    abstract void draw();


}
