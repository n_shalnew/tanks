import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2i;

public class Tank extends PhysicalObject {

    private int directionX = World.cellSize;
    private int directionY = 0;
    private int cooldown = 0;//кд на выстрел
    // boolean isPlayer = false;//игрок ли//перенести сюда
    private int speed = World.cellSize;
    private int moveCooldown = 0;//кд на смену направления ИИ
    private int goInOneTurn = 0;//переменная для движения в одну сторону
    //boolean isActive = false;//активность ии//ждёт своёго звездого часа(mb)
    private float colorR = 0;
    private float colorG = 0;
    private float colorB = 1;
//    private boolean targetLocked = false;//перенести сюда

    Tank(int x, int y, int width, int height) {
        super(x, y, width, height);
        hp = 2;
    }

    @Override
    void update(ArrayList<PhysicalObject> allObjects) {
        if (isPlayer) {
            if (cooldown > 0) {
                cooldown--;
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
                directionX = speed;
                directionY = 0;
                if (canGo(allObjects)) {
                    move(directionX, directionY);
                }
            } else if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
                directionX = -speed;
                directionY = 0;
                if (canGo(allObjects)) {
                    move(directionX, directionY);
                }
            } else if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
                directionX = 0;
                directionY = -speed;
                if (canGo(allObjects)) {
                    move(directionX, directionY);
                }
            } else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
                directionX = 0;
                directionY = speed;
                if (canGo(allObjects)) {
                    move(directionX, directionY);
                }
            }


            if (Keyboard.isKeyDown(Keyboard.KEY_F5)) {
                skyNet(allObjects);
            }

            if (Keyboard.isKeyDown(Keyboard.KEY_SPACE) && cooldown == 0) {
                Bullet bullet = new Bullet(x + directionX / 2, y + directionY / 2, World.bulletSize, World.bulletSize);
                bullet.num = num;
                bullet.setDirectionX(directionX);
                bullet.setDirectionY(directionY);

                allObjects.add(bullet);
                cooldown = 50;
            }
        }

        if (hp <= 0) {
            isRemove = true;
            if (isPlayer)
                System.exit(1);
        }
    }

    @Override
    void draw() {
        /*Новое тело танка не очень*/
          /*  glColor3f(1, 0, 1);
            glBegin(GL_LINE_LOOP);
            glVertex2i(x - wigth / 2, y - height / 2);
            glVertex2i(x + wigth / 2, y - height / 2);
            glVertex2i(x + wigth / 2, y - height / 4);
            glVertex2i(x + wigth / 4, y - height / 8);
            glVertex2i(x + wigth / 4, y + height / 8);
            glVertex2i(x + wigth / 2, y + height / 4);
            glVertex2i(x + wigth / 2, y + height / 4);
            glVertex2i(x - wigth / 2, y + height / 2);
            glVertex2i(x - wigth / 2, y + height / 4);
            glVertex2i(x - wigth / 4, y + height / 8);
            glVertex2i(x - wigth / 2, y + height / 8);
            glVertex2i(x - wigth / 2, y - height / 8);
            glVertex2i(x - wigth / 4, y - height / 8);
            glVertex2i(x - wigth / 2, y - height / 4);
            //glVertex2i(x - wigth * 3 / 14, y - height / 14);//
            glEnd();*/



         /*Тело танка*/
        glColor3f(colorR, colorG, colorB);
        glBegin(GL_POLYGON);
        glVertex2i(x - wigth / 2, y - height / 2);
        glVertex2i(x - wigth / 2, y + height / 2);
        glVertex2i(x + wigth / 2, y + height / 2);
        glVertex2i(x + wigth / 2, y - height / 2);
        glEnd();


        if (directionX > 0 && directionY == 0) {
        /*Дуло танка вправо*/
            glColor3f(1, 1, 1);
            glBegin(GL_POLYGON);
            glVertex2i(x + wigth / 2, y - height / 8);
            glVertex2i(x + wigth / 2, y + height / 8);
            glVertex2i(x + wigth, y + height / 8);
            glVertex2i(x + wigth, y - height / 8);
            glEnd();
        }
        if (directionX < 0 && directionY == 0) {
        /*Дуло танка влево*/
            glColor3f(1, 1, 1);
            glBegin(GL_POLYGON);
            glVertex2i(x - wigth / 2, y - height / 8);
            glVertex2i(x - wigth, y - height / 8);
            glVertex2i(x - wigth, y + height / 8);
            glVertex2i(x - wigth / 2, y + height / 8);
            glEnd();
        }
        if (directionX == 0 && directionY > 0) {
        /*Дуло танка вниз*/
            glColor3f(1, 1, 1);
            glBegin(GL_POLYGON);
            glVertex2i(x - wigth / 8, y + height / 2);
            glVertex2i(x - wigth / 8, y + height);
            glVertex2i(x + wigth / 8, y + height);
            glVertex2i(x + wigth / 8, y + height / 2);
            glEnd();
        }
        if (directionX == 0 && directionY < 0) {
        /*Дуло танка вверх*/
            glColor3f(1, 1, 1);
            glBegin(GL_POLYGON);
            glVertex2i(x - wigth / 8, y - height);
            glVertex2i(x - wigth / 8, y - height / 2);
            glVertex2i(x + wigth / 8, y - height / 2);
            glVertex2i(x + wigth / 8, y - height);
            glEnd();
        }

        /*Индикация здоровья*/
        if (hp == 2) {
            glColor3f(0.807f, 0.035f, 0.035f);
            glBegin(GL_POLYGON);
            glVertex2i(x - wigth / 2, y - height / 2);
            glVertex2i(x - wigth / 2, y - height / 4);
            glVertex2i(x, y - height / 4);
            glVertex2i(x, y - height / 2);
            glEnd();
        }
        if (hp == 1) {
            glColor3f(0.807f, 0.035f, 0.035f);
            glBegin(GL_POLYGON);
            glVertex2i(x - wigth / 2, y - height / 2);
            glVertex2i(x - wigth / 2, y - height / 4);
            glVertex2i(x - wigth / 4, y - height / 4);
            glVertex2i(x - wigth / 4, y - height / 2);
            glEnd();
        }

        /*Белая рамка*/
        glColor3f(1, 1, 1);
        glBegin(GL_LINE_LOOP);
        glVertex2i(x - wigth / 2, y - height / 2);
        glVertex2i(x - wigth / 2, y + height / 2);
        glVertex2i(x + wigth / 2, y + height / 2);
        glVertex2i(x + wigth / 2, y - height / 2);
        glEnd();

        if (isPlayer && targetLocked){
            glColor3f(1, 0, 0);
            glBegin(GL_LINE_LOOP);
            glVertex2i(x - wigth / 2, y - height / 2);
            glVertex2i(x - wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y - height / 2);
            glEnd();
        }
    }


    boolean canGo(ArrayList<PhysicalObject> allObjects) {
        for (int i = 0; i < allObjects.size(); i++) {

            if (getDirectionX() > 0 && (allObjects.get(i).x - x == World.cellSize) && (allObjects.get(i).y - y == 0)) {
                return false;
            }
            if (getDirectionX() < 0 && (allObjects.get(i).x - x == -World.cellSize) && (allObjects.get(i).y - y == 0)) {
                return false;
            }
            if (getDirectionY() > 0 && (allObjects.get(i).y - y == World.cellSize) && (allObjects.get(i).x - x == 0)) {
                return false;
            }
            if (getDirectionY() < 0 && (allObjects.get(i).y - y == -World.cellSize) && (allObjects.get(i).x - x == 0)) {
                return false;
            }

        }
        return true;
    }


    /*
    @param directionX направление по Х
    @param directionY направление по У
    */
    void move(int directionX, int directionY) {
        if (moveCooldown == 0) {
            this.directionX = directionX;
            this.directionY = directionY;
            x += directionX;
            y += directionY;
            moveCooldown = 10;
        } else {
            moveCooldown--;
        }
    }

    void skyNet(ArrayList<PhysicalObject> allObjects) {
        Random random = new Random();
        if (hp > 0 && !targetLocked) {
            if (goInOneTurn != 0) {
                goInOneTurn--;
            }
            if (goInOneTurn == 0) {
                int speedX;
                int speedY;
                int turn = random.nextInt(2);
                if (turn == 0) {
                    do {
                        speedY = random.nextInt(3) - 1;
                    }
                    while (speedY == 0);
                    directionY = speedY * World.cellSize;
                    directionX = 0;
                } else {
                    do {
                        speedX = random.nextInt(3) - 1;
                    }
                    while (speedX == 0);
                    directionX = speedX * World.cellSize;
                    directionY = 0;
                }
                goInOneTurn = 30;
            }

            if (cooldown > 0) {
                cooldown--;
            }
            int act = random.nextInt(30);
            if (act == 3 && cooldown == 0) {
                Bullet bullet = new Bullet(getX() + getDirectionX() / 2, getY() + getDirectionY() / 2, World.bulletSize, World.bulletSize);
                bullet.num = num;
                bullet.setDirectionX(getDirectionX());
                bullet.setDirectionY(getDirectionY());

                allObjects.add(bullet);
                cooldown = 50;
            }

            if (canGo(allObjects))
                move(directionX, directionY);

            for (int i = 0; i < allObjects.size(); i++) {
                if (Math.abs(allObjects.get(i).x - x) <= 200 && Math.abs(allObjects.get(i).y - y) <= 200 && allObjects.get(i).isPlayer) {
                    targetLocked = true;
                    allObjects.get(i).targetLocked = true;
                    System.out.println("Target found!");
                }
            }
        }

        if (hp > 0 && targetLocked) {
            if (goInOneTurn != 0) {
                goInOneTurn--;
            }
            if (goInOneTurn == 0) {
                /*Поиск пути к игроку.
                    Рассматриваются 8 ситуаций, когда нужно двигаться по диагонали 4 + 4 по прямым.
                */
                for (int i = 0; i < allObjects.size(); i++) {
                    if (allObjects.get(i).isPlayer) {
                        int destX = x - allObjects.get(i).x;
                        int destY = y - allObjects.get(i).y;
                        if ((destX < 0) && (destY < 0)) {
                            /*4*/
                            if (Math.abs(destX) > Math.abs(destY)) {
                                directionX = speed;
                                directionY = 0;
                            }
                            if (Math.abs(destX) < Math.abs(destY)) {
                                directionX = 0;
                                directionY = speed;
                            }
                        }
                        if ((destX > 0) && (destY < 0)) {
                            /*3*/
                            if (Math.abs(destX) > Math.abs(destY)) {
                                directionX = -speed;
                                directionY = 0;
                            }
                            if (Math.abs(destX) < Math.abs(destY)) {
                                directionX = 0;
                                directionY = speed;
                            }
                        }
                        if ((destX < 0) && (destY > 0)) {
                            /*2*/
                            if (Math.abs(destX) > Math.abs(destY)) {
                                directionX = speed;
                                directionY = 0;
                            }
                            if (Math.abs(destX) < Math.abs(destY)) {
                                directionX = 0;
                                directionY = -speed;
                            }
                        }
                        if ((destX > 0) && (destY > 0)) {
                            /*1*/
                            if (Math.abs(destX) > Math.abs(destY)) {
                                directionX = -speed;
                                directionY = 0;
                            }
                            if (Math.abs(destX) < Math.abs(destY)) {
                                directionX = 0;
                                directionY = -speed;
                            }
                        }
                        if (destX == 0 && destY > 0) {
                            directionX = 0;
                            directionY = -speed;
                        }
                        if (destX == 0 && destY < 0) {
                            directionX = 0;
                            directionY = speed;
                        }
                        if (destX > 0 && destY == 0) {
                            directionX = -speed;
                            directionY = 0;
                        }
                        if (destX < 0 && destY == 0) {
                            directionX = speed;
                            directionY = 0;
                        }
                    }
                }
                if (cooldown > 0) {
                    cooldown--;
                }
                int act = random.nextInt(30);
                if (act == 3 && cooldown == 0) {
                    Bullet bullet = new Bullet(getX() + getDirectionX() / 2, getY() + getDirectionY() / 2, World.bulletSize, World.bulletSize);
                    bullet.num = num;
                    bullet.setDirectionX(getDirectionX());
                    bullet.setDirectionY(getDirectionY());

                    allObjects.add(bullet);
                    cooldown = 50;
                }
                if (canGo(allObjects))
                    move(directionX, directionY);
            }
            for (int i = 0; i < allObjects.size(); i++) {
                if (Math.abs(allObjects.get(i).x - x) > 200 && Math.abs(allObjects.get(i).y - y) > 200 && allObjects.get(i).isPlayer) {
                    targetLocked = false;
                    allObjects.get(i).targetLocked = false;
                    System.out.println("Target lost!");
                }
            }
        }

    }

    void setColor(float red, float green, float blue) {
        this.colorR = red;
        this.colorG = green;
        this.colorB = blue;
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

    public int getDirectionX() {
        return directionX;
    }

    public int getDirectionY() {
        return directionY;
    }

}

