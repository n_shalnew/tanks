import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2i;

public class Bullet extends PhysicalObject {
    private int timeToLive = 80;
    private int directionX = 0;
    private int directionY = 0;

    Bullet(int x, int y, int wigth, int height) {
        super(x, y, wigth, height);
    }


    @Override
    void update(ArrayList<PhysicalObject> allObjects) {
        if (timeToLive != 0) {
            x += directionX / 20 * 5;
            y += directionY / 20 * 5;
            timeToLive--;
        }
        if (timeToLive == 0) {
            isRemove = true;
        }

        for (int i = 0; i < allObjects.size(); i++) {
            isStriked(allObjects.get(i));
        }
    }

    @Override
    void draw() {
        if (timeToLive != 0) {
            glColor3f(0.478f, 0.721f, 0.694f);
            glBegin(GL_POLYGON);
            glVertex2i(x - wigth / 2, y - height / 2);
            glVertex2i(x - wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y + height / 2);
            glVertex2i(x + wigth / 2, y - height / 2);
            glEnd();
        }
    }

    private boolean isStriked(PhysicalObject ob) {
        if ((x == ob.x) && (y == ob.y) && !(ob instanceof Bullet) && num != ob.num) {
            timeToLive = 0;
            ob.hp--;
            isRemove = true;
            return true;
        }
        return false;
    }

    public void setDirectionX(int          directionX) {
        this.directionX = directionX;
    }

    public void setDirectionY(int directionY) {
        this.directionY = directionY;
    }
}
